WTServer
=========

Instalation
------------

 1. Install this libs: https://www.microsoft.com/en-us/download/details.aspx?id=30679#
   
 2. Create a link to your instalation: mklink /D C:\Apps\WTServer\www\niki24 S:\niki24\public (or) change your niki project njinx config

 3. Copy data_mysql_5.6 content to data folder  
 
 4. Add C:\Apps\WTServer\bin to your path  
 
 6. Open wtserver manager and then on projects -> niki24 click on configuration icon, then check the enable local virtual server and save
 
 7. To enable ssl go to C:\Apps\WTServer\conf\domains.d and replace the niki24.conf with niki24.conf.ssl.back
  
Versions
------------
> **25-5-2016:**  
> Added preconfigured instalation  
> Added gitignore to the domains configs  
> Changed mariadb to mysql 5.6.30 64bits  
> Removed unused 32 bits php  
> Updated nginx to 1.10 64bits  