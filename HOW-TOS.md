WTServer - portable Nginx MariaDB Redis Php development stack for Windows
=========================================================================
A portable, preconfigured, lightweight, fast and stable server stack for developing php mysql applications on windows, based on the excellent webserver Nginx. A lighter alternative to XAMPP and WAMP.




### How to create a new Project

  ![New Project](http://wtserver.wtriple.com/how-tos/1new.png)

 - Open *WTServer Manager* by clicking the taskbar or desktop icon, then click on `New Project` icon, choose a project name, hit Enter or click `Save Project`.
 - Set the project `Live Url` if any. This should point to the production version of your project, hosted on a remote server.
 - Check `Enable Local Virtual Server` if you want to use a local test domain name like `http://projectname.local`. This is useful to test SEF links and other Nginx rewrite rules.
 - Save the project settings.
 - Go to your `c:\pathTo\WWW\projectname` folder and create your project files.
 - Browse [http://localhost](http://localhost) or `http://projectname.local`
 - Use `HeidiSql` or `Adminer` to create the project`s database
 - Test your project using different PHP versions
 - Upload or Sync the project files to the remote server, as explained below.

  ![Edit Project](http://wtserver.wtriple.com/how-tos/2edit.png)




### How to upload files or synchronize your project to a server using WinSCP:

  ![Project Buttons](http://wtserver.wtriple.com/how-tos/5project.png)

 - Open *WTServer Manager*
 - Select your project, click on `Project Setup` icon, setup the remote connection and Save.
 - Return to the project list and click on `Upload`, `Synchronize` or `Browse` icons. A WinSCP dialog will open.

  ![Sync with WinSCP](http://wtserver.wtriple.com/how-tos/7sync.png)

  ![Browse Files with WinSCP](http://wtserver.wtriple.com/how-tos/10browse.png)




### How to use COMPOSER.bat:

  ![Composer](http://wtserver.wtriple.com/how-tos/4composer.png)

[Composer](https://getcomposer.org/) is a tool for dependency management in PHP used to download and keep updated various PHP frameworks and components. After you created a new project in `c:\pathTo\WWW\projectname`, in a command line window, run commands like:


	composer create-project symfony/framework-standard-edition c:\pathTo\WWW\projectname 2.4.*

	cd c:\pathTo\WWW
	composer create-project silverstripe/installer projectname 3.1.*

	cd c:\pathTo\WWW
	composer create-project laravel/laravel projectname --prefer-dist




### How to Debug Configuration Files:

After changing the configuration files, one server might refuse start. In order to find out the reason, you need to Check Configuration Syntax:

 - In the *WTServer Manager* window, use the Check Configuration Syntax buttons to see the start up errors for Nginx and MySql
 - For PHP, if there are any startup errors, they will be shown as alert messages.
 - Also check the server logs in `WTServer\log`
 - To see the commands used to start each service, Kill all servers and restart with ``` WTServer.exe --debug``` ( Display debug messages )
 - If nothing works, delete that configuration file, or from *WTServer\conf* `nginx.conf, mysql.ini, php.ini` , kill all servers and restart WTServer.exe. The Server Manager will copy the default configuration files to WTServer\conf.




### How to Clone/Sync WTServer stack to a new machine:

You can safely move the WTServer stack without loosing any data, projects, databases or settings.
Run WTServer installer once on the new machine, then overwrite/sync the whole WTServer folder with the cloned one.

For example, syncing your work from `C:\MyWork\WTServer` on Machine1 to `D:\WTServer` on Machine2 should be done by running the installer once on Macine2 (and choosing `D:\WTServer` as destination) then overwriting `D:\WTServer` each time you need to sync your work.

If you move WTServer stack to a new machine without running the installer, PHP 5.5 and later might refuse to start, because it requires *Visual C++ Redistributable for Visual Studio 2012*. You can [install it manually from here](http://www.microsoft.com/en-us/download/details.aspx?id=30679). It will not run on windows XP.




### How to change the root directory of a project:

Your project's Nginx settings are stored in `conf\domains.d\myProject.conf`. This file`s root directive **is automatically modified** by the *WTServer manager* for portability.

If you want to use a custom root folder for your project, for example `WWW\myProject\public`, you can manually set this root directive and lock it using the comment # locked

		root "C:\PathTo\WWW\myProject\public"; # locked




### How to allow access from LAN and Internet to your local project:

In the *WTServer Manager* window, go to `Project Setup`, check `Enable Local Virtual Server`, then Save.

Edit `WTServer\conf\domains.d\projectName.conf` directly or go to `Project Setup` and click on `Edit Nginx Local Virtual Server Configuration File` icon.

  ![Edit Nginx Local Virtual Server ](http://wtserver.wtriple.com/how-tos/11lan.png)

Modify like this:

	server {
		## listen		127.0.0.1:80;
		listen			*:80;

		server_name     projectName.local projectName.com projectName.myDomain.com;

		### Access Restrictions
		allow			all;
		## deny			all;

Now `Kill` Nginx, `Start` Nginx OR `Check Nginx Configuration Syntax`.




### How to achieve the best performance for live sites:

 - Run the [WTServer installer](https://sourceforge.net/projects/wtnmp/files/latest/download) to upgrade to the latest version. The installer fixes some windows networking issues that will make Nginx super fast and also allows faster connections to the Mysql server.
 - Increase the nginx workers in `nginx.conf`:

        worker_processes auto;
        events {
            worker_connections 8096;

 - If you disable PHP XCache, XDebug and other extensions, start at least 20 php-cgi servers
 - Edit `mysql.ini` and increase `innodb_buffer_pool_size , myisam_sort_buffer_size , etc`




### How to add additional local test server names to my project:

  ![Hosts File Editor](http://wtserver.wtriple.com/how-tos/3hosts.png)

You can always use different/multiple server names for your `Local Virtual Server`. Use Hosts File Editor (the third icon) to add extra local server names like:

    127.0.0.1    projectName.dev
    127.0.0.1    projectName.test
    127.0.0.1    www.projectName.xyz

Then Edit `conf\domains.d\projectName.conf` and add

    server_name projectName.dev projectName.test www.projectName.xyz;




### How to add locations

If you want a location like `/phpMyAdmin` to serve files from a directory outside your project (but inside PHP's `open_basedir`), for example `C:/WTServer/include/phpMyAdmin` you need to edit the Nginx config file:

Edit `WTServer\conf\domains.d\projectName.conf` to set http://projectName.local/phpMyAdmin
Edit `WTServer\conf\nginx.conf` to set http://localhost/phpMyAdmin

	server {
		....
		location ~ ^/phpMyAdmin/.*\.php$ {
		        root "C:/WTServer/include";
		        try_files $uri =404;
		        include     	nginx.fastcgi.conf;
		        fastcgi_pass    php_farm;
				allow			127.0.0.1;
				allow			::1;
				deny			all;
		}
		location ~ ^/phpMyAdmin/ {
		         root "C:/WTServer/include";
		}
	}

Notice that the root directive lacks `/phpMyAdmin` because Nginx adds the URL path `/phpMyAdmin` to the root path, so the resulting directory is `C:/WTServer/include/phpMyAdmin`




### How to Install Extras:

Let`s say you want to use Nginx 1.2.x instead of the currently installed version, or PHP 5.3.x

 - Kill All servers, [Download the extra package](http://sourceforge.net/projects/wtnmp/files/extras/)
 - Unzip the extra package in c:\pathTo\WTServer\
 - PHP 5.5 and later might refuse to start, because it requires Visual C++ Redistributable for Visual Studio 2012. You can [install it manually from here](http://www.microsoft.com/en-us/download/details.aspx?id=30679). It will not run on windows XP.
