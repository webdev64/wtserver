WTServer - portable Nginx MariaDB Redis Php development stack for Windows
=========================================================================
A portable, preconfigured, lightweight, fast and stable server stack for developing php mysql applications on windows, based on the excellent webserver Nginx. A lighter alternative to XAMPP and WAMP.




***Release History***	| Nginx		| MySQL - MariaDB		| PHP				| Others
------------------------|---------------|---------------|-------------------------------|---------------
[WTServer-16.05](http://sourceforge.net/projects/wtnmp/files/WTServer-16/WTServer-16.05.exe/download)	| 1.10.0	|  10.1.13 x64	| 5.6.21 & 7.0.6 x64	| Redis composer winscp hostsEditor putty php_geoip php_imagick php_xdebug
[WTServer-16.04](http://sourceforge.net/projects/wtnmp/files/WTServer-16/WTServer-16.04.exe/download)	| 1.9.14	|  10.1.13 x64	| 5.6.20 & 7.0.5 x64	| Redis composer winscp hostsEditor putty php_geoip php_imagick php_xdebug
[WTServer-16.03](http://sourceforge.net/projects/wtnmp/files/WTServer-16/WTServer-16.03.exe/download)	| 1.9.12	|  10.1.12 x64	| 5.6.19 & 7.0.4 x64	| Redis composer winscp hostsEditor putty php_geoip php_imagick php_xdebug
[WTServer-16.02](http://sourceforge.net/projects/wtnmp/files/WTServer-16/WTServer-16.02.exe/download)	| 1.9.11	|  10.1.11 x64	| 5.6.18 & 7.0.3 x64	| Redis composer winscp hostsEditor putty php_geoip php_imagick php_xdebug
[wt-nmp-16.01](http://sourceforge.net/projects/wtnmp/files/WTServer-16/wt-nmp-16.01.exe/download)	| 1.9.9	|  10.1.10	| 5.6.17 & 7.0.2	| xdebug composer winscp hostsEditor
[wt-nmp-15.12](http://sourceforge.net/projects/wtnmp/files/wt-nmp-15/wt-nmp-15.12.exe/download)	| 1.9.9	|  10.1.9	| 5.6.16 & 7.0.1	| xdebug composer winscp hostsEditor
[wt-nmp-15.11](http://sourceforge.net/projects/wtnmp/files/wt-nmp-15/wt-nmp-15.11.exe/download)	| 1.9.7	|  5.5.5 - 10.1.9	| 5.4.45 & 5.6.16 & 7.0.0rc7	| opcache xdebug xcache composer winscp hostsEditor
[wt-nmp-15.09](http://sourceforge.net/projects/wtnmp/files/wt-nmp-15/wt-nmp-15.09.exe/download)	| 1.9.4	| 5.6.26	| 5.4.45 & 5.6.13	| opcache xdebug xcache composer winscp hostsEditor
[wt-nmp-15.06](http://sourceforge.net/projects/wtnmp/files/wt-nmp-15/wt-nmp-15.06.exe/download)	| 1.9.2	| 5.6.24	| 5.4.42 & 5.6.10	| opcache xdebug xcache composer winscp hostsEditor
[wt-nmp-15.04](http://sourceforge.net/projects/wtnmp/files/wt-nmp-15/wt-nmp-15.04.exe/download)	| 1.7.11	| 5.6.23	| 5.4.39 & 5.6.7	| opcache xdebug xcache composer winscp hostsFileEditor
[wt-nmp-15.02](http://sourceforge.net/projects/wtnmp/files/wt-nmp-15/wt-nmp-15.02.exe/download)	| 1.7.10	| 5.6.23	| 5.4.38 & 5.6.6	| opcache xdebug xcache composer winscp hostsFileEditor
[wt-nmp-15.01](http://sourceforge.net/projects/wtnmp/files/wt-nmp-15/wt-nmp-15.01.exe/download)	| 1.7.9		| 5.6.22	| 5.4.36 & 5.6.4	| opcache xdebug xcache composer winscp hostsFileEditor
[wt-nmp-14.12](http://sourceforge.net/projects/wtnmp/files/wt-nmp-14/wt-nmp-14.12.exe/download)	| 1.7.7		| 5.6.22	| 5.4.35 & 5.6.3	| opcache xdebug xcache composer node.js npm winscp hostsFileEditor
[wt-nmp-14.10](http://sourceforge.net/projects/wtnmp/files/wt-nmp-14/wt-nmp-14.10.exe/download)	| 1.7.6		| 5.6.21	| 5.4.34 & 5.5.18 & 5.6.2	| opcache xdebug xcache composer node.js npm winscp
[wt-nmp-14.01](http://sourceforge.net/projects/wtnmp/files/wt-nmp-14/wt-nmp-14.01.exe/download)	| 1.5.8		| 5.6.15	| 5.4.24 & 5.5.8	| opcache xdebug xcache
[wt-nmp-13.12](http://sourceforge.net/projects/wtnmp/files/wt-nmp-13/wt-nmp-13.12.exe/download)		| 1.5.7		| 5.6.15	| 5.4.23 & 5.5.7	| opcache xdebug xcache
[wt-nmp-13.09](http://sourceforge.net/projects/wtnmp/files/wt-nmp-13/wt-nmp-13.09.zip/download)		| 1.4.2		| 5.6.13	| 5.3.27 & 5.4.19 & 5.5.3	| opcache xdebug xcache



Changelog:
----------
* WTserver-16.02:
 - **rebranded from WT-NMP to WTserver**
 - added Redis & php_redis.dll
 - added Redis Cache Manager
 - latest versions of Nginx & MariaDB
 - modified WTserver Manager to support Redis, changed a few buttons, many optimisations
 - added a landing page for new projects
 - added 64bit versions of MariaDB, PHP7, and WTserver Manager
 - removed HeidiSql, although it is still supported, if it`s installed manually to it`s default location
 - the installer supports both x86 and x64 systems
 - tested on Windows10 x64 and Windows7 x86
 - fixed WTserver Manager for high resolution + font scalling


* wt-nmp-15.12:
 - removed PHP 5.4. It can still be added to the stack, as an extra package
 - Windows XP is no longer supported


* wt-nmp-15.11:
 - replaced MySql with MariaDB
 - fixed the Regular Expressions Tester
 - added PHP 7.0 and VC redist 2015
 - automatic database upgrade from mysql to MariaDB


* wt-nmp-15.07:
 - Set loopback ipv4 precedence over ipv6 for Windows 7

* wt-nmp-15.06:
 - removed php.ini viewer
 - Fixed integration with Hosts Editor

* wt-nmp-15.05:
 - project root lock
 - docs cleanup
 - production ready
 - increased the limit of phpCgiServers to 99

* wt-nmp-15.04:
 - High DPI optimization

* wt-nmp-15.01:
 - removed node.js
 - bugfixes
 - changed /bin directory structure

* wt-nmp-14.12:
 - added Check Configuration Syntax buttons for nginx and mysql
 - added a workaround for mysql slow connections ipv6 issue
 - integrated Local Virtual Servers using Hosts Commander and Nginx
 - implemented --wwwDir argument Custom WWW folder
 - fixed support for multiple installations pointing to the same project folder
 - improved project settings dialog
 - removed php-5.5. It is now available only as an [extra package](http://sourceforge.net/projects/wtnmp/files/extras/)

* wt-nmp-14.11 :
 - fixed several server manager issues
 - added HostsFileEditor

* wt-nmp-14.10 :
 - fixed show server manager bug
 - added support for php-5.6.x

* wt-nmp-14.09 :
 - cloned php.ini in php bin directory, for composer

* wt-nmp-14.05 :
 - wt-nmp.exe server manager now runs minimized in the system tray and monitors and restarts crashed servers
 - added Composer.phar, Node.js, NPM, and WinSCP
 - implemented Update Checker and Project List
 - implemented Project Upload, Sync and Browse with WinSCP
 - multiple upgrades and fixes.

* wt-nmp-14.03 :
 - added the latest versions of Nginx, MySQL, PHP, adminer

* wt-nmp-14.01 :
 - server manager remembers the last php version and number of php processes used
 - better support for multiple development environments on the same OS

* wt-nmp-13.12 :
 - Start servers with windows checkbox
 - Installer

* wt-nmp-13.10 :
 - added command line option -l --latestPhp
 - changed mysql connection string from 'localhost' to '127.0.0.1' to prevent 1 second delay on windows 8

* wt-nmp-13.09 :
 - added PHP 5.5.3 allongside 5.4.19 and 5.3.27
 - added opcache xdebug Php Extensions
 - in the server manager you can choose from multiple PHP versions
 - added PSPad free text editor

* wt-nmp-13.07.31 :
 - Xcache is loaded but disabled by default
 - added msmtp (sendmail alternative for windows) and php mail() tester
 - Fixed a few config bugs

* wt-nmp-13.06.09 :
 - WT-NMP is now truly portable, the wt-nmp.exe server manager updates configuration files automatically after mooving to a new location
 - MySQL data dir is moved from distribution dir mysql-*/data to wt-nmp basedir in order for database data to be persistent during upgrades
 - removed command line option -w --warningsDisabled
 - renamed command line option -p --phpProcesses to -p --phpCgiServers
 - renamed nginx additional config files to nginx.*.conf
 - moved all executables in /bin
 - added a few icons to the server manager
 - added MySQL Database daily backups --backup

* wt-nmp-13.04.23 :
 - added the latest versions of Nginx, MySQL, PHP
 - wt-nmp.exe now has a debug logBox and close button
 - all configuration files were moved to C:/wt-nmp/conf to allow easy upgrades
 - reg.php has a new look.




